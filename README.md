# [Virtus: Landing page template](http://virtusrastreamento.com.br)

This project was developed with HTML5, Sass, jQuery, and Hammer.js.

Performance basics are covered: assets are minified into single CSS and JS files, and the images are optimized.

## Misc:

* Follow Cítr.co: [Instagram](https://instagram.com/citr.co), [Facebook](https://facebook.com/citr.co)
